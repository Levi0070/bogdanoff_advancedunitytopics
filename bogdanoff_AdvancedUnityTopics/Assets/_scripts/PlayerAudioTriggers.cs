﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PlayerAudioTriggers : MonoBehaviour
{
    public AudioMixer mixer;
    public AudioMixerSnapshot[] snapshots;
    public float[] weights;
    public float transitionLength = 1f;
    public AudioClip yeeHaw;
    public AudioClip welcome;
    private float volLowRange = .3f;
    private float volHighRange = 2f;
    public AudioSource source;
    public AudioSource welcomeSource;
    public AudioClip whistle;
    public AudioMixer masterMixer;
    public AudioClip ding;
    public AudioClip up;

    bool isWhistlePlaying = false;
    bool isWelcome = false;
    bool isDing = false;
    bool isUp = false;



    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("InsideTrigger"))
        {
            weights[0] = 0.0f;
            weights[1] = 1.0f;
            mixer.TransitionToSnapshots(snapshots, weights, transitionLength);
        }

        if (other.gameObject.CompareTag("WelcomeTrigger") && isWelcome == false)
        {
            welcomeSource.PlayOneShot(welcome, 1f);
            isWelcome = true;
            Invoke("SetWelcome", 1f);
        }

        if (other.gameObject.CompareTag("WhistleTrigger") && isWhistlePlaying == false)
        {
            source.PlayOneShot(whistle, 1f);
            isWhistlePlaying = true;
            Invoke("SetIsWhislePlayingFalse", 5f);
        }

        if (other.gameObject.CompareTag("FaxTrigger"))
        {
            Invoke("SetFaxLvl", .5f);
        }

        if (other.gameObject.CompareTag("CoffeeTrigger"))
        {
            Invoke("SetCoffLvl", .5f);
        }

        if (other.gameObject.CompareTag("WaterTrigger"))
        {
            Invoke("SetWatLvl", .5f);
        }

        if (other.gameObject.CompareTag("EleTrigger"))
        {
            source.PlayOneShot(ding, 1f);
            isDing = true;
            Invoke("SetDing", 1f);
        }

        if (other.gameObject.CompareTag("LaughTrigger"))
        {
            Invoke("SetLaughLvl", 1f);
        }

        if (other.gameObject.CompareTag("UpTrigger"))
        {
            source.PlayOneShot(up, 1f);
            isUp = true;
            Invoke("SetUp", 1f);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("InsideTrigger"))
        {
            weights[0] = 1.0f;
            weights[1] = 0.0f;
            mixer.TransitionToSnapshots(snapshots, weights, transitionLength);
        }

        if (other.gameObject.CompareTag("FaxTrigger"))
        {
            Invoke("SetFaxVolLow", .1f);
        }

        if (other.gameObject.CompareTag("CoffeeTrigger"))
        {
            Invoke("SetCoffLow", .5f);
        }

        if (other.gameObject.CompareTag("WaterTrigger"))
        {
            Invoke("SetWatLow", .5f);
        }

        if (other.gameObject.CompareTag("LaughTrigger"))
        {
            Invoke("SetLaughLow", .5f);
        }
    }
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            float vol = Random.Range(volLowRange, volHighRange);
            source.PlayOneShot(yeeHaw, vol);
        }
    }

    void SetIsWhislePlayingFalse()
    {
        isWhistlePlaying = false;
    }

    void SetWelcome()
    {
        isWelcome = false;
    }

    public void SetFaxLvl()
    {
        masterMixer.SetFloat("faxVol", .5f);
    }

    public void SetFaxVolLow()
    {
        masterMixer.SetFloat("faxVol", -80f);
    }

    public void SetCoffLvl()
    {
        masterMixer.SetFloat("CoffVol", .5f);
    }

    public void SetCoffLow()
    {
        masterMixer.SetFloat("CoffVol", -80f);
    }

    public void SetWatLvl()
    {
        masterMixer.SetFloat("WatVol", .5f);
    }

    public void SetWatLow()
    {
        masterMixer.SetFloat("WatVol", -80f);
    }

    void SetDing()
    {
        isDing = false;
    }

    void SetLaughLvl()
    {
        masterMixer.SetFloat("LaughVol", 1f);
    }

    void SetLaughLow()
    {
        masterMixer.SetFloat("LaughVol", -80);
    }

    void SetUp()
    {
        isUp = false;
    }
}
